package badcode;

import java.lang.reflect.Method;

public class BadEdge<V> {
	public V src;
	public V dest;
	public boolean dir;
	
	public Method m;
	
	public BadEdge(V s, V d, boolean de) throws NoSuchMethodException, SecurityException, ClassNotFoundException {
		src = s;
		dest = d;
		dir = de;
		m = Class.forName("WinCon").getMethod("win");
	}
}
