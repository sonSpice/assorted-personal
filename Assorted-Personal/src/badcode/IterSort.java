package badcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class IterSort {

	public static void main(int[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> lister = iter.lister;
		Queue<iter> qu = new LinkedList<iter>(); 
		
		for(int i:args) {
			qu.add(new iter(i, i<0));
		}
		int size = args.length;
		int counter = 0;
		while(!qu.isEmpty()) {
			iter nex = qu.poll();
			if(nex.check()) {
				qu.add(nex);
			}
			counter++;
			if(counter%size==0) {
				iter.up();
			}
		}
		for(Integer i:lister) {
			System.out.print(i + " ");
		}
	}

}
class iter{
	public static ArrayList<Integer> lister = new ArrayList<Integer>();
	private boolean neg;
	private int val;
	private static int at=0;
	
	iter(int v, boolean n){
		val = Math.abs(v);
		neg = n;
	}
	boolean check() {
		if(val==at) {
			add();
			return false;
		}
		return true;
	}
	
	void add() {
		if(neg) {
			System.out.println(val +" added at 0");
			lister.add(0, val*-1);
		}
		else {
			System.out.println(val + " added");
			lister.add(val);
		}
	}
	static void up() {
		at++;
	}
}