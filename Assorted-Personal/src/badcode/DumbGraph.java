package badcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class DumbGraph<V> {
	public HashMap<V,LinkedList<BadEdge<V>>> adjMap;
	public ArrayList<V> nods;
	
	
	public DumbGraph() {
		adjMap = new HashMap<V, LinkedList<BadEdge<V>>>();
		nods = new ArrayList<V>();
	}
	
	public void put(V nod) throws NoSuchMethodException, SecurityException, ClassNotFoundException {
		adjMap.put(nod, new LinkedList<BadEdge<V>>());
		for(V node:nods) {
			link(nod, node, true);
		}
		nods.add(nod);
		
	}
	
	public void link(V src, V dest, boolean b) throws NoSuchMethodException, SecurityException, ClassNotFoundException {
		if(!adjMap.containsKey(src)) {
			put(src);
		}
		if(!adjMap.containsKey(dest)) {
			put(dest);
		}
		addEdge(src,dest,b);
		if(b) {
			addEdge(src,dest,b);
		}
	}
	
	private void addEdge(V src, V dest, boolean d) throws NoSuchMethodException, SecurityException, ClassNotFoundException {
		adjMap.get(src).add(new BadEdge<V>(src,dest,d));
	}
	
	public boolean calc() {
		HashMap<TicTacToePiece, Boolean> vis = new HashMap<TicTacToePiece, Boolean>();
		
		
		return false;
	}
	
	public void traverse() {
		
	}
}
