package connect4;

public class ConEdge {

	public ConNode src;
	public ConNode dest;
	public char line;
	
	public ConEdge(ConNode s, ConNode d, char l) {
		src = s;
		dest = d;
		line = l;
	}
	
}
