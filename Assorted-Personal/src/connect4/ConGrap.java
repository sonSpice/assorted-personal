package connect4;

import java.util.LinkedList;

public class ConGrap {

	private ConNode[][] mesh;
	private LinkedList<ConEdge>[][] adjMess;

	public ConGrap() {
		
		//2D Array of pieces on the board, for adjacency.
		mesh = new ConNode[6][7];
		
		//2D Array of pieces' adjacencies
		adjMess = new LinkedList[6][7];
	}

	private boolean link(int x1, int y1, int x2, int y2, char l) {
		
		/*If piece exists, link piece. Char l in edge is supposed to filter what
		kind of win condition line would be drawn from that piece, so that all
		adjacencies can exist within one Graph, and line counting win conditions
		can be implemented with a simple filtered DFS
		 */
		if(!(mesh[y2][x2]==null)) {
			adjMess[y1][x1].add(new ConEdge(mesh[y1][x1],mesh[y2][x2], l));
			adjMess[y2][x2].add(new ConEdge(mesh[y2][x2],mesh[y1][x1], l));
			return true;
		}
		return false;
	}

	public boolean place(int x, int y, char pl) {
		mesh[y][x] = new ConNode(x,y,pl);
		adjMess[y][x] = new LinkedList<ConEdge>();
		
		/*These booleans represent what directions there is room on the board to look
		without index out of bounds. Used to simplify diagonal checking*/
		boolean[] av = {false, false, false, false};

		//Horizontal Adj
		if(x-1>=0) {
			link(x,y,x-1,y,'h');
			//Left
			av[0] = true;
		}
		if(x+1<7) {
			link(x,y,x+1,y,'h');
			//Right
			av[1] = true;
		}

		//Vert Adj
		if(y-1>=0) {
			link(x,y,x,y-1,'v');
			//Down
			av[2] = true;
		}
		if(y+1<6) {
			link(x,y,x,y+1,'v');
			//Up
			av[3] = true;
		}

		//Diag
		//LD X
		if(av[0]&&av[2]) {
			link(x,y,x-1,y-1,'x');
		}
		//LU Y
		if(av[0]&&av[3]) {
			link(x,y,x-1,y+1,'y');
		}
		//RD Y
		if(av[1]&&av[2]) {
			link(x,y,x+1,y-1,'y');
		}
		//RU X
		if(av[1]&&av[3]) {
			link(x,y,x+1,y+1,'x');
		}		
		return true;
	}
	
	private int bT;
	private int wT;
	/**Method takes in the position of the last piece, and from
	 * the position of the last piece, checks one specific win condition
	 * from that point, both checking if a win was made, or a block was made.
	 * 
	 * @param x X Coord of Piece
	 * @param y Y Coord of Piece
	 * @param pl Player char of piece
	 * @param l The win condition line being checked
	 * @return Returns if a win condition was met.
	 */
	public boolean checkL(int x, int y, char pl, char l) {
		//Line Vars
		boolean w = false;
		wT = 1;
		bT = 1;
		
		//Init Vis
		boolean[][] vis = new boolean[6][7];
		for(int i = 0; i<6;i++) {
			for(int j = 0; j<7;j++) {
				vis[i][j] = false;
			}
		}
		//For Win
		checkUt(vis,x,y,pl,l);
		if(wT>=4) w = true;
		
		//Reset Vis
		for(int i = 0; i<6;i++) {
			for(int j = 0; j<7;j++) {
				vis[i][j] = false;
			}
		}
		
		
		
		//For Block
		blockUt(vis,x,y,pl,l);
		if(bT>=4) System.out.println("Block!");
		return w;
	}
	public void checkUt(boolean[][] v, int x, int y, char pl, char l) {
		
		//Set Node to Visited
		v[y][x] = true;
		
		
		/*For all Edges from this node
		 * If the edge is correllated to the correct win condition
		 * and the destination is an unvisited same piece, continue DFS
		 */
		for(ConEdge ed:adjMess[y][x]) {
			if(l == ed.line) { //If same line
				ConNode nod = ed.dest;
				if(!v[nod.y][nod.x] && pl == nod.pl) { //If unvisited matching
					wT++;
					checkUt(v, nod.x,nod.y,pl,l);
				}
			}
		}
	}
	
	//Works exactly the same as checkUt
	public void blockUt(boolean[][] v, int x, int y, char pl, char l) {
		//System.out.println("Running block " +x + y);
		v[y][x] = true;
		for(ConEdge ed:adjMess[y][x]) {
			
			if(l == ed.line) {
				ConNode nod = ed.dest;
				
				
				if(!v[nod.y][nod.x] && pl != nod.pl) {
					bT++;
					blockUt(v, nod.x,nod.y,pl,l);
				}
			}
		}
	}
	
}
