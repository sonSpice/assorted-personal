package connect4;

public class ConNode {

	public int x;
	public int y;
	public char pl;
	
	public ConNode(int a, int b, char c) {
		x = a;
		y = b;
		pl = c;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public char getPL() {
		return pl;
	}
}
