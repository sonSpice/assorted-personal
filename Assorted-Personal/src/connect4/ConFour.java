package connect4;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConFour {
	/*This is a completely normal game of Connect 4
	
		Do not ask how this works. 
		This is a normal game 
		of connect 4.
		
	*/
	
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		String in;
		boolean pl = true;
		while(pl) {
			play();
			System.out.println("Do you want to play agan?");
			in = scan.next();
			if(!in.toLowerCase().contains("y") && !in.toLowerCase().contains("s")) {
				pl = false;
			}
		}
		
	}

	//Game Exec methods
	private static ConGrap grap;
	public static void play() {
		String[][] board = new String[6][7];
		setBoard(board);
		printBoard(board);
		
		
		boolean contin = true;
		boolean contin2 = true;
		int turn = 1;
		char[] pls = {'X', 'O'};
		
		//Init New Graph
		grap = new ConGrap();
		
		//Run Turns until Victory
		while(contin && contin2) {
			contin = !turn(pls[turn%2],board,0);
			for(int p = 0; p<6; p++) {
				System.out.println();
			}
			turn++;
			contin2 = avail(board);
		}
		
		if(contin2) System.out.println(pls[(turn-1)%2] +" Wins!");
		else System.out.println("Tie! How did this happen?");
	}
	
	//This just looks for free spaces
	public static boolean avail(String[][] board) {
		boolean Spaces = false;
		for(int i = 0; i<board.length;i++) {
			for(int j = 0;j<board[i].length;j++) {
				if(board[i][j].equals("* ")) return true;
			}
		}
		return false;
	}
	
	//Outputs whether the turn ended in a win
	public static boolean turn(char pl, String[][] board, int att) {
		if(att>0 && att<3) {
			System.out.println("Try again");
		}
		else if(att>=3 && att<6) {
			System.out.println("Please");
		}
		else if(att>=6) {
			System.out.println("I am begging you");
		}
		
		//Get input
		Scanner sc = new Scanner(System.in);
		int x;
		System.out.println("Give num 0-6 inclusive");
		
		//Get X Cord
		try {
		x = sc.nextInt();
		}
		catch(Exception e) {
			//Case One, input mismatch probably
			System.out.println("No");
			return turn(pl,board,att+1);
		}
		if(x>6 || x<0) {
			//Case Two, input out of bounds
			System.out.println("No");
			return turn(pl,board,att+1);
		}
		
		
		
		//Get Y Cord
		int y=-1;
		for(int i=0;i<6;i++) {
			if(board[i][x].equals("* ")) y = i;
			else break;
		}
		if(y == -1) {
			//Case Three, column full
			System.out.println("No");
			return turn(pl,board,att+1);
		}
		
		//Place Piece
		board[y][x] = pl +" ";
		grap.place(x, y, pl);
		
		
		//Return win value
		return check(board,pl,x,y);
	}
	
	
	public static boolean check(String[][] b, char pl, int x, int y) {
		printBoard(b);
		boolean win = false;
		char[] lines = {'h','v','x','y'};
		
		//For all lines to check, check the lines. Do not ask how this works.
		for(char l:lines) {
			
			if(grap.checkL(x, y, pl, l)) {
				win = true;
			}
			
		}
		return win;
	}
	
	
	
	
	//Simple Useful methods
	
	public static void setBoard(String[][] b) {
		for(int i=0;i<b.length;i++) {
			for(int j=0;j<b[0].length;j++) {
				b[i][j] = "* ";
			}
		}
	}
	
	public static void printBoard(String[][] b) {
		for(int i=0;i<b.length;i++) {
			for(int j=0;j<b[0].length;j++) {
				System.out.print(b[i][j]);
			}
			System.out.println();
		}
	}
}
